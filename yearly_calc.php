#! /usr/bin/env php
<?php namespace Acme;
use Acme\current_year;
use Symfony\Component\Console\Application;



require 'vendor/autoload.php';

$app = new application();
$app->add(new current_year);

/*$app->register('sayHello')->addArgument('name')->setCode(function(InputInterface $input,OutputInterface $output ){
    $output->writeln('Hello World');
});*/

$app->run();