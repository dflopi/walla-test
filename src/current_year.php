<?php namespace Acme;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Acme;

 class Current_Year extends Command{
    public function configure(){
        date_default_timezone_set('UTC');
        $this->setName('print_year')
        ->setDescription('Offer a fullYear salary dates Calender');
    }
    public function execute(InputInterface $input, OutputInterface $output){
        $str = $this->print_year();
        $output->writeln($str);
    }
    public function print_year(){
        $year_start = date('Y-01-01');
        $year_end = date('Y-01-01', strtotime('+1 year'));
        $date_range = $this->createDateSalaryArray($year_start,$year_end);
        return $date_range;
    }

    public function createDateSalaryArray($strDateFrom,$strDateTo)
     {
         $aryRange=array();
         $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
         $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
         if ($iDateTo>=$iDateFrom)
         {
             //For long term i would have built an DayType Interface so we could have applied to S.O.L.I.D.
             while ($iDateFrom<$iDateTo)
             {
               switch($this->day_type($iDateFrom)){
                    case "Salary_day":
                        array_push($aryRange,'Salary Day: '.$this->get_csv_format($iDateFrom));
                    break;
                    case "Salary_day_off":
                        $new_work_day = $this->move_salary_day_from_a_day_off(date('Y-m-d',$iDateFrom));
                        array_push($aryRange,'Salary Day * : '.$this->get_csv_format($new_work_day));
                    break;
                    case "regular day":
                        //Do nothing.
                    break;
                    case "bonus_day_off":
                        $new_bonus_day = $this->move_bonus_day_from_a_day_off(date('Y-m-d',$iDateFrom));
                        array_push($aryRange,'bonus day * : '.$this->get_csv_format($new_bonus_day));
                        break;
                    case "bonus_day":
                        array_push($aryRange,'bonus day: '.$this->get_csv_format($iDateFrom));
                        break;
                }
                 $iDateFrom+=86400;
             }
         }
         $this->writeToCsv($aryRange);
        // return $aryRange;

     }
     public function get_csv_format($date){
         return date('Y-m-d',$date).' '.date('l',$date);
     }
    public function writeToCsv($aryRange){
        $obj= new WriteToCsv();
        $obj->write($aryRange);
    }
     /**
      *
      * Returns @daytype
      */
     public function move_salary_day_from_a_day_off($date){
        return strtotime("last thursday",strtotime($date));
     }
     public function move_bonus_day_from_a_day_off($date){
         return strtotime("next wednesday",strtotime($date));
     }


     public function day_type($date){
         $salary_date_time = strtotime(date('Y-m-10',$date)); //Constant 10 of the month.
         $current_date_time = strtotime(date('Y-m-d',$date));
         if($salary_date_time == $current_date_time){
             if($this->is_day_off($date))
                 return "Salary_day_off";
             return "Salary_day";
         }

         $bonus_date_time = strtotime(date('Y-m-15',$date)); //Constant 15 Monthly Bonus
         if($bonus_date_time == $current_date_time){
             if($this->is_day_off($date))
                 return "bonus_day_off";
             return "bonus_day";
         }
     }
     public function is_day_off($date){
         return (date("w",$date)==5 || date("w",$date)==6) ? true : false;
     }

}